CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Rel Attributes Filter (rel_attributes_filter) module provides filters for the ckeditor to add rel attributes to links.
Currently, three filters are supported: noreferrer, noopener and nofollow.

REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.


CONFIGURATION
-------------

 * Navigate to

   Administration » Configuration » Content Authoring » Text formats and editors

   - Chose the text format to apply;

   - Chose the text format to apply;

   - Save configuration.

MAINTAINERS
-----------

Current maintainers:
 * Nelson Alves (nelson-alves) - https://www.drupal.org/u/nelson-alves

This project has been sponsored by:
 * NTT DATA
   NTT DATA – a part of NTT Group – is a trusted global innovator of IT and business services headquartered in Tokyo.
   NTT is one of the largest IT services provider in the world and has 140,000 professionals, operating in more than 50 countries.
   NTT DATA supports clients in their digital development through a wide range of consulting and strategic advisory services, cutting-edge technologies, applications, infrastructure, modernization of IT and BPOs.
   We contribute with vast experience in all sectors of economic activity and have extensive knowledge of the locations in which we operate.
